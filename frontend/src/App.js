import React from "react";
import "chartjs-plugin-streaming";
import './App.css';
import './components/Charts/apexChart'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'

import axios from "axios";

class App extends React.Component {
    state = {
        details: [],
        user: "",
        quote: "",
    };
  
    componentDidMount() {
        let data;
  
        axios
            .get("http://localhost:8000/home/")
            .then((res) => {
                data = res.data;
                this.setState({
                    details: data,
                });
            })
            .catch((err) => {});
    }
    render() {
        return (
        <MapContainer center={[51.505, -0.09]} zoom={13} scrollWheelZoom={true}>
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={[51.505, -0.09]}>
              <Popup>
                A pretty CSS3 popup. <br /> Easily customizable.
              </Popup>
            </Marker>
          </MapContainer> 
        );
    }
}

export default App;