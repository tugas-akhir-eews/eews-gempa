from django.db import models
# from jsonfield import JSONField

# Create your models here.
class React(models.Model):
    name = models.CharField(max_length=30)
    detail = models.CharField(max_length=500)
    datas = models.TextField(max_length=50000)
    
class Mseed_Data(models.Model):
    network = models.CharField(null=True, max_length=30)
    station = models.CharField(null=True, max_length=30)
    location = models.CharField(null=True, max_length=150)
    channel = models.CharField(null=True, max_length=30)
    starttime = models.CharField(null=True, max_length=50)
    endtime = models.CharField(null=True, max_length=50)
    sampling_rate = models.FloatField(null=True)
    delta = models.FloatField(null=True)
    npts = models.IntegerField(null=True)
    calib = models.FloatField(null=True)
    data = models.TextField(null=True, max_length=161535)