from http.client import HTTPResponse
from django.shortcuts import render
from rest_framework.views import APIView
from . models import *
from rest_framework.response import Response
from django.http.response import JsonResponse
from rest_framework import status
from . serializer import *
import pyrebase
import os
from obspy import read
import threading
# Create your views here.

config = {
    'apiKey': "AIzaSyCEqcGgkzQtRSRbO46TUp5usEgz3ypBuWo",
  'authDomain': "eews-storage.firebaseapp.com",
  'projectId': "eews-storage",
  'storageBucket': "eews-storage.appspot.com",
  'messagingSenderId': "52178356451",
  'appId': "1:52178356451:web:4127aee99c7a095c77ddd6",
  'measurementId': "G-52VHVCCQT4",
  "databaseURL": ""
}

firebase = pyrebase.initialize_app(config)
storage = firebase.storage()

def get_data_of_mseed(mseed):
    list_traces = read(mseed)
    list_trace = [{"network":list_trace.stats.network, "station":list_trace.stats.station, "location":list_trace.stats.location,
                   "channel":list_trace.stats.channel, "starttime":list_trace.stats.starttime, "endtime":list_trace.stats.endtime,
                   "sampling_rate":list_trace.stats.sampling_rate, "delta":list_trace.stats.delta, "npts":list_trace.stats.npts,
                   "calib":list_trace.stats.calib, "data":list_trace.data}
                  for list_trace in list_traces]
    return list_trace

def retrieve_mseed():
    jateng_mseed = storage.child('Jateng/').list_files()
    jatim_mseed = storage.child('Jatim/').list_files()
    jateng_dangkal_mseeed = storage.child('Jatengdangkal/').list_files()
    jatim_dangkal_mseeed = storage.child('Jatimdangkal/').list_files()
    databaru2019_mseed = storage.child('databaru2019/').list_files()
    for file_jateng in jateng_mseed:
        try:
            storage.child(file_jateng.name).download("storage/Jateng/" + file_jateng.name)
        except:
            print("download failed")

class ReactView(APIView):
    serializer_class = ReactSerializer
    def get(self, request):
        detail = [ {"network": detail.network,"station": detail.station, "location": detail.location,
                    "channel": detail.channel, "starttime":detail.starttime, "endtime":detail.endtime,
                    "sampling_rate":detail.sampling_rate, "delta": detail.delta, "npts": detail.npts,
                    "calib": detail.calib, "data": detail.data} 
        for detail in Mseed_Data.objects.all()]
        print(Mseed_Data.objects.count())
        return Response(detail)
    
    def post(self, request):
        storage.child('Jateng/20090104_002031.mseed').download("storage/Jateng/20090104_002031.mseed")
        datas = read("storage/Jateng/20090104_002031.mseed")
        print(datas)
        for detail in datas:
            serializer = ReactSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.validated_data['network'] = detail.stats.network
                serializer.validated_data['station'] = detail.stats.station
                serializer.validated_data['channel'] = detail.stats.channel
                serializer.validated_data['starttime'] = detail.stats.starttime
                serializer.validated_data['endtime'] = detail.stats.endtime
                serializer.validated_data['sampling_rate'] = detail.stats.sampling_rate
                serializer.validated_data['delta'] = detail.stats.delta
                serializer.validated_data['npts'] = detail.stats.npts
                serializer.validated_data['calib'] = detail.stats.calib
                serializer.validated_data['data'] = detail.data
                serializer.save()
            # os.remove("storage/Jateng/20090104_002031.mseed")
        return Response(serializer.data)
        
    def delete(self, request):
        Mseed_Data.objects.all().delete()
        return Response()
        # return JsonResponse({'message': '{} Tutorials were deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)