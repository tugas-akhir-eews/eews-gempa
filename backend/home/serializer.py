from rest_framework import serializers
from . models import *
  
class ReactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mseed_Data
        fields = ['network', 'station', 'location', 'channel',
                  'starttime', 'endtime', 'sampling_rate', 'delta', 
                  'npts', 'calib', 'data']