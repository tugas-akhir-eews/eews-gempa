# Generated by Django 3.2.16 on 2022-10-07 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Mseed_Data',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('network', models.CharField(max_length=30, null=True)),
                ('station', models.CharField(max_length=30, null=True)),
                ('location', models.CharField(max_length=150, null=True)),
                ('channel', models.CharField(max_length=30, null=True)),
                ('starttime', models.CharField(max_length=50, null=True)),
                ('endtime', models.CharField(max_length=50, null=True)),
                ('sampling_rate', models.FloatField(null=True)),
                ('delta', models.FloatField(null=True)),
                ('npts', models.IntegerField(null=True)),
                ('calib', models.FloatField(null=True)),
                ('data', models.TextField(max_length=16535, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='React',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('detail', models.CharField(max_length=500)),
                ('datas', models.TextField(max_length=50000)),
            ],
        ),
    ]
